package com.ing.zoo.animals;

import com.ing.zoo.interfaces.Herbivore;

public class Hippo extends Animal implements Herbivore {

    public Hippo(String name) {
        super(name);
        setHelloText("splash");
        setEatLeavesText("munch munch lovely");
    }

    @Override
    public void sayHello() {
        System.out.println(getName() + ": " + getHelloText());
    }

    @Override
    public void eatLeaves() {
        System.out.println(getName() + ": " + getEatLeavesText());
    }
}
