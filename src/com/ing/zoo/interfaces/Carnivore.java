package com.ing.zoo.interfaces;

public interface Carnivore {
    public abstract void eatMeat();
}
