package com.ing.zoo.animals;

import com.ing.zoo.interfaces.Carnivore;
import com.ing.zoo.interfaces.Herbivore;

import java.util.ArrayList;

public class Elephant extends Animal implements Herbivore, Carnivore {
    private ArrayList<String> tricks = new ArrayList<String>();

    public Elephant(String name) {
        super(name);
        setHelloText("Trumpets!");
        setEatLeavesText("munch munch trumpets!");
        setEatMeatText("nomnomnom trumpets thx!");
    }

    @Override
    public void sayHello() {
        System.out.println(getName() + ": " + getHelloText());
    }

    @Override
    public void eatLeaves() {
        System.out.println(getName() + ": " + getEatLeavesText());
    }

    @Override
    public void eatMeat() {
        System.out.println(getName() + ": " + getEatMeatText());
    }

}
