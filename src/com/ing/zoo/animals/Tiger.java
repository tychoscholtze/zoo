package com.ing.zoo.animals;

import com.ing.zoo.interfaces.Carnivore;
import com.ing.zoo.interfaces.Trickster;

import java.util.ArrayList;
import java.util.Random;

public class Tiger extends Animal implements Carnivore, Trickster {
    private ArrayList<String> tricks = new ArrayList<String>();

    public Tiger(String name) {
        super(name);

        setHelloText("rraaarww");
        setEatMeatText("nomnomnom oink wubalubadubdub");

        tricks.add("jumps in tree");
        tricks.add("scratches ears");
    }

    @Override
    public void sayHello() {
        System.out.println(getName() + ": " + getHelloText());
    }

    @Override
    public void eatMeat() {
        System.out.println(getName() + ": " + getEatMeatText());
    }

    @Override
    public void performTrick() {
        Random random = new Random();
        int rnd = random.nextInt(tricks.size());

        System.out.println(getName() + ": " + tricks.get(rnd));
    }

    @Override
    public ArrayList<String> getTricks() {
        return tricks;
    }

    @Override
    public void addTrick(String trickDescription) {
        this.tricks.add(trickDescription);
    }

    @Override
    public void removeTrick(int trickID) {
        this.tricks.remove(trickID);
    }
}
