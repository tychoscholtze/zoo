package com.ing.zoo.animals;

import com.ing.zoo.interfaces.Herbivore;

public class Zebra extends Animal implements Herbivore {
    public String eatText;

    public Zebra(String name) {
        super(name);
        setHelloText("zebra zebra");
    }

    @Override
    public void sayHello() {
        System.out.println(getName() + ": " + getHelloText());
    }

    @Override
    public void eatLeaves() {
        eatText = "munch munch zank yee bra";
        System.out.println(getName() + ": " + eatText);
    }
}
