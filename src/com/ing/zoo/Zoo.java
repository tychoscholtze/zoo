package com.ing.zoo;

import com.ing.zoo.animals.*;
import com.ing.zoo.interfaces.Carnivore;
import com.ing.zoo.interfaces.Herbivore;
import com.ing.zoo.interfaces.Trickster;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Zoo {
    static final String cmdHello = "hello";
    static final String cmdGive = "give";
    static final String cmdPerform = "perform";

    static final String giveLeaves = "leaves";
    static final String giveMeat = "meat";
    static final String perfTrick = "trick";

    static final String unkownItem = "Unkown item '%s'\r\n";
    static final String unkownPerformance = "Unkown performance '%s'\r\n";
    static final String insufficientArguments = "Insuficient arguments for command '%s'\r\n";
    static final String invalidTarget = "Action '%s' could not be applied to target '%s'\r\n";
    static final String targetDoesNotExist = "Target '%s' does not exist\r\n";
    static final String unkownCommand = "Unkown command '%s'\r\n";

    public static void main(String[] args) {
        HashMap<String, Animal> animals = new HashMap<>();

        animals.put("henk", new Lion("henk"));
        animals.put("elsa", new Hippo("elsa"));
        animals.put("dora", new Pig("dora"));
        animals.put("wally", new Tiger("wally"));
        animals.put("marty", new Zebra("marty"));
        animals.put("anne", new Elephant("anne"));
        animals.put("bolt", new Chita("bolt"));

        Scanner scanner = new Scanner(System.in);
        System.out.print("Voer uw command in: ");

        String input[] = scanner.nextLine().split(" ");

        switch (input[0]) {
            case cmdHello:
                if (input.length > 1) {
                    if (animals.containsKey(input[1])) {
                        animals.get(input[1]).sayHello();
                    } else {
                        System.out.printf(targetDoesNotExist, input[1]);
                    }
                } else {
                    for (Map.Entry entry : animals.entrySet()) {
                        ((Animal) entry.getValue()).sayHello();
                    }
                }
                break;
            case cmdGive:
                if (input.length > 1) {
                    switch (input[1]) {
                        case giveLeaves:
                            if (input.length > 2) {
                                if (animals.containsKey(input[2])) {
                                    Animal animal = animals.get(input[2]);
                                    if (animal instanceof Herbivore) {
                                        ((Herbivore) animal).eatLeaves();
                                    } else {
                                        System.out.printf(invalidTarget, cmdGive + ' ' + giveLeaves, animal.getName());
                                    }
                                } else {
                                    System.out.printf(targetDoesNotExist, input[2]);
                                }
                            } else {
                                for (Map.Entry entry : animals.entrySet()) {
                                    Object animal = (Object) entry.getValue();

                                    if (animal instanceof Herbivore) {
                                        ((Herbivore) animal).eatLeaves();
                                    }
                                }
                            }
                            break;
                        case giveMeat:
                            if (input.length > 2) {
                                if (animals.containsKey(input[2])) {
                                    Animal animal = animals.get(input[2]);
                                    if (animal instanceof Carnivore) {
                                        ((Carnivore) animal).eatMeat();
                                    } else {
                                        System.out.printf(invalidTarget, cmdGive + ' ' + giveMeat, animal.getName());
                                    }
                                } else {
                                    System.out.printf(targetDoesNotExist, input[2]);
                                }
                            } else {
                                for (Map.Entry entry : animals.entrySet()) {
                                    Object animal = (Object) entry.getValue();

                                    if (animal instanceof Carnivore) {
                                        ((Carnivore) animal).eatMeat();
                                    }
                                }
                            }
                            break;
                        default:
                            System.out.printf(unkownItem, input[1]);
                            break;
                    }
                } else {
                    System.out.printf(insufficientArguments, cmdGive);
                }
                break;
            case cmdPerform:
                if (input.length > 1) {
                    switch (input[1]) {
                        case perfTrick:
                            if (input.length > 2) {
                                if (animals.containsKey(input[2])) {
                                    Animal animal = animals.get(input[2]);
                                    if (animal instanceof Trickster) {
                                        ((Trickster) animal).performTrick();
                                    } else {
                                        System.out.printf(invalidTarget, cmdPerform + ' ' + perfTrick, animal.getName());
                                    }
                                } else {
                                    System.out.printf(targetDoesNotExist, input[2]);
                                }
                            } else {
                                for (Map.Entry entry : animals.entrySet()) {
                                    Object animal = (Object) entry.getValue();

                                    if (animal instanceof Trickster) {
                                        ((Trickster) animal).performTrick();
                                    }
                                }
                            }
                            break;
                        default:
                            System.out.printf(unkownPerformance, input[1]);
                            break;
                    }
                } else {
                    System.out.printf(insufficientArguments, cmdPerform);
                }
                break;
            default:
                System.out.printf(unkownCommand, input[0]);
                break;
        }
    }
}
