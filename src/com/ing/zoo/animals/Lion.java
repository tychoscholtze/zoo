package com.ing.zoo.animals;

import com.ing.zoo.interfaces.Carnivore;

public class Lion extends Animal implements Carnivore {

    public Lion(String name) {
        super(name);
        setHelloText("roooaoaaaaar");
        setEatMeatText("nomnomnom thx mate");
    }

    @Override
    public void sayHello() {
        System.out.println(getName() + ": " + getHelloText());
    }

    @Override
    public void eatMeat() {
        System.out.println(getName() + ": " + getEatMeatText());
    }
}
