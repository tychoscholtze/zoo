package com.ing.zoo.animals;

public abstract class Animal {
    private String name;
    private String helloText;
    private String eatMeatText;
    private String eatLeavesText;

    Animal(String name) {
        setName(name);
    }

    public abstract void sayHello();

    public String getEatLeavesText() {
        return eatLeavesText;
    }

    public String getEatMeatText() {
        return eatMeatText;
    }

    public String getName() {
        return name;
    }

    public String getHelloText() {
        return helloText;
    }

    public void setEatLeavesText(String eatLeavesText) {
        this.eatLeavesText = eatLeavesText;
    }

    public void setEatMeatText(String eatMeatText) {
        this.eatMeatText = eatMeatText;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHelloText(String helloText) {
        this.helloText = helloText;
    }
}
