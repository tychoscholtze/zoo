package com.ing.zoo.animals;

import com.ing.zoo.interfaces.Carnivore;

public class Chita extends Animal implements Carnivore {

    public Chita(String name) {
        super(name);
        setHelloText("Chirrup");
        setEatMeatText("Nom nom nom");
    }

    @Override
    public void sayHello() {
        System.out.println(getName() + ": " + getHelloText());
    }

    @Override
    public void eatMeat() {
        System.out.println(getName() + ": " + getEatMeatText());
    }
}
