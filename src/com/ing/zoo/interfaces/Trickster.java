package com.ing.zoo.interfaces;

import java.util.ArrayList;

public interface Trickster {
    public abstract void performTrick();

    public ArrayList<String> getTricks();

    public void addTrick(String trickDescription);

    public void removeTrick(int trickID);
}
